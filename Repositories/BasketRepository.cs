﻿using Basket.API.Entities;
using Basket.API.Repositories.Interfaces;
using Microsoft.Extensions.Caching.Distributed;
using Newtonsoft.Json;

namespace Basket.API.Repositories;

/// <inheritdoc/>
public class BasketRepository : IBasketRepository
{
    private readonly IDistributedCache _redisCache;
    
    public BasketRepository(IDistributedCache cache)
    {
        _redisCache = cache ?? throw new ArgumentNullException(nameof(cache));
    }
    
    public async Task<ShoppingCart?> GetBasket(string userName)
    {
        var basketJson = await _redisCache.GetStringAsync(userName);
        
        return string.IsNullOrEmpty(basketJson)
            ? null
            : JsonConvert.DeserializeObject<ShoppingCart>(basketJson);
    }
         
    public async Task<ShoppingCart?> UpdateBasket(ShoppingCart basket)
    {
        var basketJson = JsonConvert.SerializeObject(basket);
        await _redisCache.SetStringAsync(basket.UserName, basketJson);
             
        return await GetBasket(basket.UserName);
    }
    
    public Task DeleteBasket(string userName) 
        => _redisCache.RemoveAsync(userName);
}