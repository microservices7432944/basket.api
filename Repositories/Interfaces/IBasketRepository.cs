﻿using Basket.API.Entities;

namespace Basket.API.Repositories.Interfaces;

/// <summary>
/// Репозиторий корзины
/// </summary>
public interface IBasketRepository
{
    /// <summary>
    /// Получить корзину
    /// </summary>
    /// <param name="userName">Имя пользователя</param>
    Task<ShoppingCart?> GetBasket(string userName);
    
    /// <summary>
    /// Обновить корзину
    /// </summary>
    /// <param name="basket">Корзина</param>
    Task<ShoppingCart?> UpdateBasket(ShoppingCart basket);
    
    /// <summary>
    /// Удалить корзину
    /// </summary>
    /// <param name="userName">Имя пользователя</param>
    Task DeleteBasket(string userName);
}