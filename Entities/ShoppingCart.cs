﻿namespace Basket.API.Entities;

/// <summary>
/// Корзина
/// </summary>
public class ShoppingCart
{
    /// <summary>
    /// Имя пользователя
    /// </summary>
    public string UserName { get; set; }
    
    /// <summary>
    /// Элементы корзины
    /// </summary>
    public List<ShoppingCartItem> Items { get; set; } = new();

    public ShoppingCart(string userName)
    {
        UserName = userName;
    }
    
    /// <summary>
    /// Сумма
    /// </summary>
    public decimal TotalPrice 
    {
        get
        {
            return Items.Sum(item => item.Price * item.Quantity);
        }
    }
}