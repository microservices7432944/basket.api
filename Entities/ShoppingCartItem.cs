﻿namespace Basket.API.Entities;

/// <summary>
/// Элемент корзины
/// </summary>
public class ShoppingCartItem
{
    /// <summary>
    /// Количество
    /// </summary>
    public int Quantity { get; set; }
    
    /// <summary>
    /// Цвет
    /// </summary>
    public required string Color { get; set; }
    
    /// <summary>
    /// Цена
    /// </summary>
    public decimal Price { get; set; }
    
    /// <summary>
    /// Идентификатор продукта
    /// </summary>
    public required string ProductId { get; set; }
    
    /// <summary>
    /// Наименование продукта
    /// </summary>
    public required string ProductName { get; set; }
}