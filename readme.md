# Basket.API

### Простой API микросервис корзины продуктов c использованием Redis.

### Стек:
1. .Net 7 / AspNetCore
2. Redis
3. Docker

Сервис для личного ознакомления с docker-compose, микросервисной архитектурой.
Источник: https://medium.com/aspnetrun/microservices-architecture-on-net-3b4865eea03f

Запускалось в среде WSL2/Ubuntu + Docker desktop.

Запуск: _`docker-compose -f docker-compose.yml -f docker-compose.development.yml up`_